# MithMail - A demo mail client written in Mithril #

<div align="center">

<img src="https://gitlab.com/mrman/mithmail/raw/master/mithmail-demo.gif" alt="Demo GIF"></img>
</div>

This project goes with the [Mithril SystemJS and Parcel guide post over @ vadosware.io](https://vadosware.io/post/mithril-systemjs-and-parcel-getting-started-guide). It's meant to be a demo of how to run the relatively simple combination of [Mithril](https://mithril.js.org/), [SystemJS](https://github.com/systemjs/systemjs)
, and [Parcel](https://parceljs.org/) all together to write modern [Single Page Applications (SPAs)](https://en.wikipedia.org/wiki/Single-page_application).

Here are the pieces and how they fit together:

- **Mithril**: minimal yet capable component-driven library (more complete than others, since it includes routing and AJAX facilities)
- **SystemJS**: module loader
- **Parcel**: bundler

For more background on these tools, check out their documentation:

- [Mithril](https://mithril.js.org/)
- [SystemJS (v2.0+)](https://github.com/systemjs/systemjs)
- [Parcel](https://parceljs.org/)

## Development ##

Development work flow consists of starting the following:

```shell
$ yarn parcel
$ emacs -nw <file> # the lightest operating system you'll ever use which also happens to edit text
```

If you're looking at the code in the parts before ParcelJS was introduced:
```shell
$ yarn build-watch # in the background/another window so you can see output
$ yarn serve # in the background/another window
```

## FAQ ##

**How come the early commits mentioned Rollup [Rollup](https://rollupjs.org/)?**

Well half way through I decided after fiddling with Parcel and it's various plugins that facilitate front end development I realized how bad the usage story was in comparison to [Parcel](https://parceljs.org) (which I've used in the past) -- so I decided to use parcel for the main branch and make an offshoot that switches to Parcel (if I do it at all). The concrete advantages of Parcel over Parcel seem to be few and far between these days so while I like Parcel (or more precisely I am a fan of [SystemJS](https://github.com/systemjs/systemjs) and the integration it had with [JSPM](https://jspm.org/) of old), I'm not going to torture myself (and by proxy any readers) setting up Parcel (I'm doing it from scratch) when Parcel offers an easy and relatively equally functional alternative.
