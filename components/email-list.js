import m from "../static/vendor/mithril/1.1.6/mithril.min.js";
import EmailSvc from "../services/email.js";

const ELLIPSIS_CHAR_LIMIT = 60;

const EmailListItem = {
  view: function(vnode) {
    const email = vnode.attrs.email;
    const textContent = email.text || "";
    const formattedSendDate = email.sendDate.toLocaleString();

    const isSelected = EmailSvc.getEmailSelectedStatusById(email.id);
    const isChecked = EmailSvc.getEmailCheckedStatusById(email.id);

    let additionalClasses = [];
    if (isSelected) { additionalClasses.push("selected"); }

    return m(
      ".component.email-list-item.clickable",
      {
        class: additionalClasses.join(" "),
        onclick: () => EmailSvc.toggleSelectionStatusById(email.id),
      },
      [
        m(".header", [
          m(".lhs", [
            m("input[type=checkbox]", {
              checked: isChecked,
              onclick: () => EmailSvc.toggleCheckedStatusById(email.id),
            }),
            m("span.sm-margin-left.sender", email.from.name),
            m("span.sm-margin-left.tag", email.label),
          ]),

          m(".rhs", [
            m(".date", formattedSendDate)
          ])
        ]),

        m(".blurb.md-margin-top", [
          m("p.subject", email.subject),
          // This is not a great way to do ellipsis but... it'll work for the demo.
          m("p.body-preview", textContent.substring(0, ELLIPSIS_CHAR_LIMIT) + "..."),
        ]),

      ]);
  }
};

const EmailList = {
  oninit: function() {
    this.selectedEmails = {};
  },

  view: function(vnode) {
    const emails = vnode.attrs.emails || [];

    return m(".component.email-list", emails.map(function(email) {
      return m(EmailListItem, {
        email: email,
        onEmailSelect: (email) => this.selectedEmails[email.id] = true,
      });
    }));
  }
};

export default EmailList;
