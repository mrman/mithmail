import m from "../static/vendor/mithril/1.1.6/mithril.min.js";
import LHSNav from  "./lhs-nav.js";

// Use all the imports to actualy do work
const MainLayout = {
  view: function(vnode) {
    return m(".component.app-page", [
      m(LHSNav),
      vnode.children,
    ]);
  },
};

export default MainLayout;
