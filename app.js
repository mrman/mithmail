import m from "./static/vendor/mithril/1.1.6/mithril.min";
import EmailSvc from "./services/email.js";

import MainLayout from "./components/main-layout";
import WithRouteTriggeredLoader from "./components/with-route-triggered-loader";
import InboxMail from "./components/inbox-mail";

// Set up the app
m.route(document.body, "/inbox", {

  "/:subpage": {
    onmatch: function(args) {

      // NOTE: Using onmatch for code split functionality *and* using `render` for the main inbox route doesn't work (flickers result)
      // this is the best way to get clean /inbox, /starred, etc routes, without excessive re-rendering/flicker
      switch(args.subpage) {
      case "starred":
        return import("./components/starred-mail")
          .then(({default: StarredMail}) => StarredMail);

      case "important":
        return import("./components/important-mail")
          .then(({default: ImportantMail}) => ImportantMail);

      case "draft":
        return import("./components/draft-mail")
          .then(({default: DraftMail}) => DraftMail);

      case "inbox":
      default:
        return {
          view: () => m(MainLayout, m(WithRouteTriggeredLoader, {
            title: "Loading inbox...",
            // This function provides the data
            loadFn: function() { return EmailSvc.getEmails(); },
            // This function provides the component to WithRouteTriggeredLoader
            childFn: function(data) { return m(InboxMail, {emails: data}); }
          }))
        };
      }
    }
  },

});
