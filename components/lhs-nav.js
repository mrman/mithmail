import m from "../static/vendor/mithril/1.1.6/mithril.min.js";

import InboxWhiteIcon from "../static/images/icons/inbox-white.svg";
import StarFullWhiteIcon from "../static/images/icons/star-full-white.svg";
import FileLineWhiteIcon from "../static/images/icons/file-line-white.svg";
import ExclamationTriangleWhiteIcon from "../static/images/icons/exclamation-triangle-white.svg";
import PencilWhiteIcon from "../static/images/icons/pencil-white.svg";

var LHS_NAV_ITEMS = [
  {
    title: "Inbox",
    iconImgSrc: InboxWhiteIcon,
    badgeCSSBgColor: "#2285c6",
    badgeContent: 2,
    route: "/inbox",
  },

  {
    title: "Starred",
    iconImgSrc: StarFullWhiteIcon,
    route: "/starred",
  },

  {
    title: "Draft",
    iconImgSrc: FileLineWhiteIcon,
    route: "/draft",
  },

  {
    title: "Important",
    iconImgSrc: ExclamationTriangleWhiteIcon,
    route: "/important",
  },

];

var LHS_LABEL_ITEMS = [
  {
    title: "Work",
    badgeCSSBgColor: "#02bfc0",
    badgeEmptySquare: true,
  },

  {
    title: "Private",
    badgeCSSBgColor: "#f9d47b",
    badgeEmptySquare: true,
  },

  {
    title: "Priority",
    badgeCSSBgColor: "#9391e5",
    badgeEmptySquare: true,
  },
];

var HeaderButton = {
  view: function(vnode) {
    var text = vnode.attrs.title || "Compose Mail";

    return m("button", {class: "component header-button"}, [
      m("span", text),
      m("img", {class: "sm-margin-left icon", src: PencilWhiteIcon}),
    ]);
  }
};

var LHSNavHeader = {
  view: function() {
    return m("header", [
      m(HeaderButton),
    ]);
  }
};

var Badge = {
  view: function(vnode) {
    var styles = [];
    var content = vnode.attrs.content || "";
    var bgColor = vnode.attrs.cssBgColor || "gray";
    var color = vnode.attrs.cssBgColor || "white";
    var emptySquare = vnode.attrs.emptySquare;
    var classes = ["component", "badge"];

    styles.push("background-color:" + vnode.attrs.cssBgColor);
    styles.push("color:" + vnode.attrs.cssColor);
    var combinedStyles = styles.join(";");

    if (emptySquare) { classes.push("empty-square"); }

    return m("div", {class: classes.join(" "), style: combinedStyles}, content);
  }
};

var NavItem = {
  view: function(vnode) {
    var data = vnode.attrs.data || {};
    var title = data.title || "Placeholder";
    var iconImgSrc = data.iconImgSrc;
    var badgeContent = data.badgeContent || 0;
    var badgeCSSBgColor = data.badgeCSSBgColor;
    var badgeEmptySquare = data.badgeEmptySquare;
    var children = [title];
    var classes = ["component", "nav-item", "md-padding-horiz"];

    if (data.selected) { classes.push("selected"); }

    // If there's an icon specified display it
    if (iconImgSrc) {
      children.unshift(m("img", {class: "sm-margin-left sm-margin-right icon", src: iconImgSrc}));
    } else {
      children.unshift(m("div", {class: "placeholder-1em"}));
    }

    // If badgeColor and badgeContent are specified then show a badge
    if (badgeEmptySquare && badgeCSSBgColor || badgeContent && badgeCSSBgColor) {
      children.push(m("div", {class: "badge-container float-right"}, [
        m(Badge, {
          class: "float-right",
          cssBgColor: badgeCSSBgColor,
          content: badgeContent,
          emptySquare: badgeEmptySquare
        })
      ]));
    }

    return m("li.clickable", {
      class: classes.join(" "),
      onclick: function(e) {
        // Navigate to the appropriate link for this
        m.route.set(data.route);
      },
    }, children);
  }
};


var NavItemListing = {
  view: function(vnode) {
    var navItems = vnode.attrs.items || [];
    var children = navItems.map(function(i) {
      return m(NavItem, {data: i});
    });

    return m("ul", {class: "component nav-item-listing"}, children);
  }
};

var LHSNavPageList = {
  view: function() {
    // Mark the nav items with the correct selected property
    var currentPath = m.route.get();
    var navItems = LHS_NAV_ITEMS.map(function(item) {
      item.selected = currentPath === item.route;
      return item;
    });

    return m(NavItemListing, {items: navItems});
  }
};

var LHSNavLabelList = {
  view: function() {
    var labelItems = LHS_LABEL_ITEMS;
    return m(NavItemListing, {items: labelItems});
  }
};

var LHSNav = {
  view: function() {
    return m("div", {class: "component lhs-nav"}, [
      m(LHSNavHeader),
      m(LHSNavPageList),
      m("hr"),
      m("h2", {class: "slim-text sm-padding-horiz"}, "Labels"),
      m(LHSNavLabelList),
      m("hr"),
    ]);
  }
};

export default LHSNav;
