import m from "../static/vendor/mithril/1.1.6/mithril.min";

import EmailSvc from "../services/email";

import RHSHeader from "./rhs-header";
import MailPageSubNav from "./mail-page-subnav";
import EmailList from "./email-list";
import MainLayout from "./main-layout";
import WithRouteTriggeredLoader from "./with-route-triggered-loader";

const DraftMail = {
  view: function(vnode) {
    const newCount = vnode.attrs.newCount;

    return m(MainLayout,
             m(WithRouteTriggeredLoader, {
               title: "Loading draft emails...",
               // This function provides the data
               loadFn: function() { return EmailSvc.getEmails(); },
               // This function provides the component to WithRouteTriggeredLoader
               childFn: (data) => m(".component.draft-rhs", [
                 // Colored header
                 m(RHSHeader, {class: "draft-gray-bg white-fg", title: "Draft", newCount: newCount}),

                 // Container with buttons
                 m(MailPageSubNav),

                 // EmailList
                 m(EmailList, {emails: data}),
               ])
             })
            );
  },
};

export default DraftMail;
