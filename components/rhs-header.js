import m from "../static/vendor/mithril/1.1.6/mithril.min.js";

import SettingsWhiteIcon from "../static/images/icons/settings-white.svg";
import ExampleAvatar from "../static/images/example-avatar.jpg";

const RHSHeader = {
  view: function(vnode) {
    const title = vnode.attrs.title || "Title";
    const bgColor = vnode.attrs.bgColor || "#2285c6";
    const newCount = vnode.attrs.newCount;

    return m("div.component.rhs-header.flex", {class: vnode.attrs.class}, [
      m(".title-container", [
        m("span.title", title),
        newCount ? m("span.new-count", "(" + newCount + ")"): undefined,
      ]),

      m(".controls", [
        m("input.search-box[type='text'][placeholder='Search']"),
        m("img.sm-margin-left.icon", {src: SettingsWhiteIcon}),
        m("img.sm-margin-left.avatar", {src: ExampleAvatar}),
      ])
    ]);
  }
};

export default RHSHeader;
