import m from "../static/vendor/mithril/1.1.6/mithril.min";

import EmailSvc from "../services/email";

import RHSHeader from "./rhs-header";
import MailPageSubNav from "./mail-page-subnav";
import EmailList from "./email-list";
import MainLayout from "./main-layout";
import WithRouteTriggeredLoader from "./with-route-triggered-loader";

const ImportantMail = {
  view: function(vnode) {
    const newCount = vnode.attrs.newCount;

    return m(MainLayout,
             m(WithRouteTriggeredLoader, {
               title: "Loading important emails...",
               // This function provides the data
               loadFn: function() { return EmailSvc.getEmails(); },
               // This function provides the component to WithRouteTriggeredLoader
               childFn: (data) => m(".component.important-rhs", [
                 // Colored header
                 m(RHSHeader, {class: "important-red-bg white-fg", title: "Important", newCount: newCount}),

                 // Container with buttons
                 m(MailPageSubNav),

                 // EmailList
                 m(EmailList, {emails: data}),
               ])
             })
            );
  }
};

export default ImportantMail;
