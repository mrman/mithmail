import m from "../static/vendor/mithril/1.1.6/mithril.min.js";
import EmailSvc from "../services/email.js";

import SquareIcon from "../static/images/icons/square.svg";
import MinusSquareIcon from "../static/images/icons/minus-square.svg";
import CheckSquareIcon from "../static/images/icons/check-square.svg";
import ChevronDownIcon from "../static/images/icons/chevron-down.svg";
import ChevronLeftIcon from "../static/images/icons/chevron-left.svg";
import ChevronRightIcon from "../static/images/icons/chevron-right.svg";

const MultiSelectCheckbox = {
  view: function(vnode) {
    const allEmailsChecked = vnode.attrs.allEmailsChecked;
    const oneOrMoreEmailsChecked = vnode.attrs.oneOrMoreEmailsChecked;

    const dropdownContentClasses = [];
    const dropdownContentHidden = "dropdownContentHidden" in vnode.attrs ? vnode.attrs.dropdownContentHidden : true;
    if (dropdownContentHidden) { dropdownContentClasses.push("hidden"); }

    let selectionIconSrc = SquareIcon;
    if (oneOrMoreEmailsChecked) { selectionIconSrc = MinusSquareIcon; }
    if (allEmailsChecked) { selectionIconSrc = CheckSquareIcon; }

    let selectionText = "Select All";
    if (oneOrMoreEmailsChecked) { selectionText = "Clear Selection"; }
    if (allEmailsChecked) { selectionText = "Unselect All"; }

    let clickHandler = function() { EmailSvc.checkAllEmails(); };
    if (allEmailsChecked || oneOrMoreEmailsChecked) { clickHandler = function() { EmailSvc.clearCheckedEmails(); }; }

    return m(
      "span.component.multi-select-checkbox.gray-boxed.clickable",
      {onclick: clickHandler},
      [
        m("span", [
          m("img.icon", {src: selectionIconSrc}),
          m("span.xs-margin-left", selectionText),
        ]),
      ]);
  }
};

const ButtonGroup = {
  view: function(vnode) {
    return m("span.component.button-group", {class: vnode.attrs.class}, vnode.children);
  }
};

const DropDownButton = {
  view: function(vnode) {
    const title = vnode.attrs.title || "Button";

    return m("button.component.drop-down-button.gray-boxed.clickable", [
      m("span", title),
      m("img.xs-margin-left.icon", {src: ChevronDownIcon}),
    ]);
  }
};

const PaginationControls = {
  view: function(vnode) {
    const title = vnode.attrs.title || "Button";
    const lowerBound = 0;
    const upperBound = 50;
    const boundStatement = lowerBound + " - " + upperBound;
    const total = 100;

    return m(".component.pagination-controls", [
      m("strong", boundStatement),
      m("span", " of "),
      m("strong", total),
      m("img.xs-margin-left.icon-lg.gray-boxed.valign-mid", {src: ChevronLeftIcon}),
      m("img.xs-margin-left.icon-lg.gray-boxed.valign-mid", {src: ChevronRightIcon}),
    ]);
  }
};

const MailPageSubNav = {
  view: function(vnode) {
    const allEmailsChecked = EmailSvc.allEmailsChecked();
    const oneOrMoreEmailsChecked = EmailSvc.oneOrMoreEmailsChecked();

    // Container with buttons
    return m(".component.mail-page-subnav", [
      m(".lhs", [
        m(MultiSelectCheckbox, {
          allEmailsChecked: allEmailsChecked,
          oneOrMoreEmailsChecked: oneOrMoreEmailsChecked,
        }),

        m(ButtonGroup, {class: "sm-margin-left"}, [
          m("button.clickable", "Archive"),
          m("button.clickable", "Spam"),
          m("button.clickable", "Delete"),
        ]),

        m(ButtonGroup, {class: "sm-margin-left"}, [
          m(DropDownButton, {title: "Move to"}),
          m(DropDownButton, {title: "Label"}),
        ]),
      ]),

      m(".rhs", [
        m(PaginationControls)
      ]),
    ]);
  },
};

export default MailPageSubNav;
