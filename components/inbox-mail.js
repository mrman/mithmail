import m from "../static/vendor/mithril/1.1.6/mithril.min";
import RHSHeader from "./rhs-header";
import MailPageSubNav from "./mail-page-subnav";
import EmailList from "./email-list";

const InboxMail = {
  view: function(vnode) {
    const newCount = vnode.attrs.newCount;
    const emails = vnode.attrs.emails;

    return m(".component.inbox-rhs", [
      // Colored header
      m(RHSHeader, {class: "inbox-blue-bg white-fg", title: "Inbox", newCount: newCount}),

      // Container with buttons
      m(MailPageSubNav),

      // EmailList
      m(EmailList, {emails: vnode.attrs.emails}),
    ]);
  }
};

export default InboxMail;
