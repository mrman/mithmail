import m from "../static/vendor/mithril/1.1.6/mithril.min.js";

var ONE_MINUTE_MS = 1000 * 60;
var ONE_HOUR_MS = 60 * ONE_MINUTE_MS;
var ONE_DAY_MS = 24 * ONE_HOUR_MS;
var ONE_WEEK_MS = 27 * ONE_DAY_MS;

function randomPastDate() {
  var mins = Math.floor(Math.random() * 60) * ONE_MINUTE_MS;
  var hours = Math.floor(Math.random() * 24) * ONE_HOUR_MS;
  var days = Math.floor(Math.random() * 7) * ONE_DAY_MS;
  return new Date(new Date().getTime() - mins - hours - days);
}

var EMAILS = [
  {
    id: "QyLruCuUQ4SOy0JTkOuJ3VdOk8HbDuw3",
    from: {name: "Victor Erixon", email: "victor.erixon@example.com"},
    subject: "Theories of Design",
    text: "Molestias deserunt veritatis modi ut fugiat sint. Dolorum vel autem sint. Dolores accusamus dolores occaecati tenetur adipisci asperiores",
    sendDate: randomPastDate(),
  },
  {
    id: "KpjgJQT8nIBdLj1NCjCPPvZJBtzhhixn",
    from: {name: "Jacob Hubertus", email: "jacob.hubertus@example.com"},
    subject: "Street Photography",
    text: "Molestias deserunt veritatis modi ut fugiat sint. Dolorum vel autem sint. Dolores accusamus dolores occaecati tenetur adipisci asperiores",
    sendDate: randomPastDate(),
  },
  {
    id: "JiTTyuQ2XUakNd6xSeolyoWEpeNjOwJi",
    from: {name: "August Berglund", email: "august.berglund@example.com"},
    subject: "HTML+CSS Tutorials?",
    text: "Molestias deserunt veritatis modi ut fugiat sint. Dolorum vel autem sint. Dolores accusamus dolores occaecati tenetur adipisci asperiores",
    sendDate: randomPastDate(),
  },
  {
    id: "XlwnoP5EUNiauVn8anBba1Ui2NeSwUCg",
    from: {name: "Rick Brunstedt", email: "rick.brunstedt@example.com"},
    subject: "Train Crossing",
    text: "Molestias deserunt veritatis modi ut fugiat sint. Dolorum vel autem sint. Dolores accusamus dolores occaecati tenetur adipisci asperiores",
    sendDate: randomPastDate(),
  },
  {
    id: "w5smg1YFSo9ANXZQe56bQkNxcX1mJK16",
    from: {name: "Webydo Team", email: "team@webydo.com"},
    subject: "Does Webydo have what you're looking for? ",
    text: "Molestias deserunt veritatis modi ut fugiat sint. Dolorum vel autem sint. Dolores accusamus dolores occaecati tenetur adipisci asperiores",
    sendDate: randomPastDate(),
  },
];

EMAILS = EMAILS.sort(function(a, b) {
  var aTime = a.sendDate.getTime();
  var bTime = b.sendDate.getTime();
  if (aTime < aTime) { return -1; }
  if (bTime > bTime) { return 1; }
  return 0;
});


/**
 * Email service that will manage checking
 *
 * Note: Mithril can't redraw subtrees yet (https://github.com/MithrilJS/mithril.js/issues/1907), so m.redraw() is everywhere
 */
function EmailService() {
  this.selectedEmailId = null;
  this.checkedEmailIds = {};
  this._allEmailsChecked = false;
}

EmailService.prototype.selectEmailById = function(id, opts) {
  if (!id) { throw new Error("Invalid email object, ID is missing"); }

  console.log("[email-svc] selected email with id:", id);
  this.selectedEmailId = id;
  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.resetSelectedEmailId = function(opts) {
  this.selectedEmailId = null;
  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.toggleSelectionStatusById = function(id, opts) {
  if (!id) { throw new Error("Invalid email object, ID is missing"); }

  // Reset if currently selected and exit early
  if (this.getEmailSelectedStatusById(id)) {
    this.resetSelectedEmailId();
    return;
  }

  this.selectEmailById(id);

  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.getEmailSelectedStatusById = function(id) {
  if (!id) { throw new Error("Invalid email object, ID is missing"); }
  return this.selectedEmailId === id;
};

EmailService.prototype.checkEmailById = function(id, opts) {
  if (!id) { throw new Error("Invalid email object, ID is missing"); }

  console.log("[email-svc] checked email with id:", id);
  this.checkedEmailIds[id] = true;
  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.resetEmailCheckedStatusById = function(id, opts) {
  if (!id) { throw new Error("Invalid email object, ID is missing"); }

  delete this.checkedEmailIds[id]; // This is kinda bad to do a lot
  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.toggleCheckedStatusById = function(id, opts) {
  if (!id) { throw new Error("Invalid email object, ID is missing"); }

  // Reset if currently selected and exit early
  if (this.getEmailCheckedStatusById(id)) {
    this.resetEmailCheckedStatusById(id);
    return;
  }

  this.checkEmailById(id);

  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.getEmailCheckedStatusById = function(id) {
  if (!id) { throw new Error("Invalid email object, ID is missing"); }

  return this._allEmailsChecked ? true : id in this.checkedEmailIds;
};

EmailService.prototype.clearCheckedEmails = function(opts) {
  this._allEmailsChecked = false;
  this.checkedEmailIds = {};

  console.log("[email-svc] cleared checked emails");
  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.checkAllEmails = function(opts) {
  this._allEmailsChecked = true;

  console.log("[email-svc] checked all emails");
  if (opts && opts.redraw) { m.redraw(); }
};

EmailService.prototype.allEmailsChecked = function(opts) {
  return this._allEmailsChecked;
};

EmailService.prototype.oneOrMoreEmailsChecked = function(opts) {
  // Object.keys isn't supported on some IE browsers, but Mithril doesn't support older than IE11 anyway IIRC
  return this._allEmailsChecked || Object.keys(this.checkedEmailIds).length > 0;
};

EmailService.prototype.getEmails = function() {
  var randomWaitMs = 1000 + Math.floor(Math.random() * 3) * 1000;

  return new Promise(function(resolve, reject) {
    // After some random wait, return the global EMAILS we've always used
    setTimeout(function() {
      resolve(EMAILS);
    }, randomWaitMs);
  });
};

export default new EmailService();
