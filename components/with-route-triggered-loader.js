import m from "../static/vendor/mithril/1.1.6/mithril.min.js";

import LoaderGrayIcon from "../static/images/icons/loader-gray.svg";

const LoadingIndicator = {
  view: function(vnode) {
    const title = vnode.attrs.title || "Loading...";

    return m(".component.loading-indicator", [
      m("img.icon.loading-icon", {src: LoaderGrayIcon}),
      m(".title", title),
    ]);
  },
};


const WithRouteTriggeredLoader = {
  oninit: function(vnode) {
    this.loadFn = vnode.attrs.loadFn;
    this.requestInFlight = false;

    this.refetchOnUpdate = true;
    if ("refetchOnUpdate" in vnode.attrs) { this.refetchOnUpdate = vnode.attrs.refetchOnUpdate; }

    // Trigger initial fetch
    this.fetchAndRedraw();
  },

  fetchAndRedraw: function() {
    this.loaded = false;
    this.loadedData = null;
    // Trigger redraw with emptied data
    // NOTE: this redraw doesn't work with onmatch (since we're code splitting) versus `render`
    // It will render two components on the page instead of one
    // m.redraw();

    this.requestInFlight = true;
    (this.loadFn ? this.loadFn() : Promise.resolve())
      .then(data => {
        this.requestInFlight = false;
        this.loadedData = data;
        this.loaded = true;
        this.currentRoute = m.route.get();
        m.redraw();
      });
  },

  onupdate: function(vnode) {
    const newRoute = m.route.get();
    const currentRoute = this.currentRoute;

    // If the route has been updated & refetching on update is turned on then re load
    if (newRoute != currentRoute && this.refetchOnUpdate && !this.requestInFlight) {
      this.fetchAndRedraw();
    }
  },

  view: function(vnode) {
    const loader = m(LoadingIndicator, {title: vnode.attrs.title});

    return m(".component.with-loader", [
      this.loaded ? vnode.attrs.childFn(this.loadedData) : loader
    ]);
  },
};

export default WithRouteTriggeredLoader;
